const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

// Allow cross-origin requests
app.use(cors());

// Connect to DB
mongoose.connect('mongodb://localhost:27017/GraphQL', {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  connectTimeoutMS: 10000
});

mongoose.connection.once('open', () => {
  console.log('MongoDB is connected');
});

//
app.use(
  '/graphql',
  graphqlHTTP({
    schema,
    graphiql: true
  })
);

const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`Server listening on ${port}`));
