import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import React from 'react';
import Addbook from './components/Addbook';
// Components
import BookList from './components/BookList';

// Apollo client setup
const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
});

function App() {
  return (
    <ApolloProvider client={client}>
      <div id="main">
        <h1>Full Stack NodeJs ReactJs GraphQL</h1>
        <BookList></BookList>
        <Addbook></Addbook>
      </div>
    </ApolloProvider>
  );
}

export default App;
