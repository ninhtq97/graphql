import { useQuery } from '@apollo/react-hooks';
import React from 'react';
import { getBookQuery } from '../queries/queries';

function BookDetails({ bookId }) {
  const { data } = useQuery(getBookQuery, {
    variables: { id: bookId }
  });

  const { book } = data || [];

  const displayBookDetails = () => {
    if (book) {
      return (
        <div>
          <h2>{book.name}</h2>
          <p>{book.genre}</p>
          <p>{book.author.name}</p>
          <p>All books by this author:</p>
          <ul className="other-books">
            {book.author.books.map(item => (
              <li key={item.id}>{item.name}</li>
            ))}
          </ul>
        </div>
      );
    } else {
      return <div>No book selected...</div>;
    }
  };

  return (
    <div id="book-details">
      <p>Book details here</p>
      {displayBookDetails()}
    </div>
  );
}

export default BookDetails;
