import { useQuery } from '@apollo/react-hooks';
import React, { useState } from 'react';
import { getBooksQuery } from '../queries/queries';
import BookDetails from './BookDetails';

function BookList() {
  const { loading, data } = useQuery(getBooksQuery);

  const { books } = data || [];

  const [selected, setSelected] = useState(null);

  const showBookDetailsHandle = (id, e) => {
    setSelected(id);
  };

  const displayBook = () => {
    if (loading) {
      return <div>Loading...</div>;
    } else {
      return books.map(book => (
        <li key={book.id} onClick={showBookDetailsHandle.bind(this, book.id)}>
          {book.name}
        </li>
      ));
    }
  };

  return (
    <>
      <ul id="book-list">{displayBook()}</ul>
      <BookDetails bookId={selected}></BookDetails>
    </>
  );
}

export default BookList;
