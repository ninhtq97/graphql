import { useMutation, useQuery } from '@apollo/react-hooks';
import React from 'react';
import useInput from '../hooks/useInput';
import {
  addBookMutation,
  getAuthorsQuery,
  getBooksQuery
} from '../queries/queries';

function Addbook() {
  // States
  const [name, bindName, resetName] = useInput('');
  const [genre, bindGenre, resetGenre] = useInput('');
  const [authorId, bindAuthorId, resetAuthorId] = useInput('');

  // Query GraphQL
  const { loading, data } = useQuery(getAuthorsQuery);
  const { authors } = data || [];

  // Function
  const [addBook] = useMutation(addBookMutation);

  const submitForm = e => {
    e.preventDefault();

    addBook({
      variables: { name, genre, authorId },
      refetchQueries: [{ query: getBooksQuery }]
    });

    resetName();
    resetGenre();
    resetAuthorId();
  };

  return (
    <>
      <p>Add Book</p>
      <form id="add-book" onSubmit={submitForm.bind(this)}>
        <div className="field">
          <label>Book name:</label>
          <input type="text" {...bindName} />
        </div>
        <div className="field">
          <label>Genre:</label>
          <input type="text" {...bindGenre} />
        </div>
        <div className="field">
          <label>Author:</label>
          <select {...bindAuthorId}>
            <option value="">Select author</option>
            {loading ? (
              <option value="" disabled>
                Loading Authors...
              </option>
            ) : (
              authors.map(author => (
                <option value={author.id} key={author.id}>
                  {author.name}
                </option>
              ))
            )}
          </select>
        </div>
        <button>+</button>
      </form>
    </>
  );
}

export default Addbook;
